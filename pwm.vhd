LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.std_logic_unsigned.all;

entity pwm is
	generic(
		sys_clk         : integer := 50_000_000; --system clock frequency in Hz
		pwm_freq        : integer := 100_000; --PWM switching frequency in Hz
		bits_resolution : integer := 8); --bits of resolution setting the duty cycle
	port(
		clk     : in  std_logic;        --system clock
		reset_n : in  std_logic;        --asynchronous reset
		enable  : in  std_logic;        --latches in new duty cycle
		value   : in  std_logic_vector(bits_resolution - 1 downto 0); --duty cycle
		pwm_out : out std_logic);       --pwm outputs
end pwm;

ARCHITECTURE logic OF pwm IS
	CONSTANT period  : integer                       := sys_clk / pwm_freq; --number of clocks in one pwm period
	SIGNAL count     : integer RANGE 0 TO period - 1; -- Period counter
	SIGNAL half_duty : integer RANGE 0 TO period / 2 := 0; --number of clocks in 1/2 duty cycle
BEGIN
	PROCESS(clk, reset_n)
	BEGIN
		IF (reset_n = '0') THEN         --asynchronous reset
			count <= 0;                 --clear counter
			pwm_out <= '0';             --clear pwm outputs
	ELSIF (rising_edge(clk)) THEN   --rising system clock edge
			IF (enable = '1') THEN      --latch in new duty cycle
				half_duty <= conv_integer(value) * period / (2 ** bits_resolution) / 2; --determine clocks in 1/2 duty cycle
			END IF;
			IF (count = period - 1) THEN --end of period reached
				count <= 0;             --reset counter
			ELSE                        --end of period not reached
				count <= count + 1;     --increment counter
			END IF;
			--control outputs for each phase
			IF (count = half_duty) THEN --phase's falling edge reached
				pwm_out <= '0';         --deassert the pwm output
			ELSIF (count = period - half_duty) THEN --phase's rising edge reached
				pwm_out <= '1';         --assert the pwm output
			END IF;
		END IF;
	END PROCESS;
END logic;
