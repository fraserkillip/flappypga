library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity ground is
	port(
		state          : in  game_state;
		clk, rom_clock : in  std_logic;
		hpos           : in  integer range 0 to 1280;
		vpos           : in  integer range 0 to 1024;
		RGB            : out std_logic_vector(11 downto 0);
		draw           : out std_logic
	);
end entity ground;

architecture ground_arch of ground is
	-- Rom signals
	SIGNAL rom_data    : STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL rom_address : STD_LOGIC_VECTOR(12 DOWNTO 0);

	-- Hight of Y position of ground
	constant ypos : integer               := 960;
	-- Current x offset of ground
	signal xpos   : integer range 0 to 63 := 0;

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(12 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
		);
	END COMPONENT;
begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "ground.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 8192,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 13,
			width_a                => 13,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => rom_clock,
			address_a => rom_address,
			q_a       => rom_data
		);

	-- Update the grounds position
	process(clk, state)
	begin
		if (rising_edge(clk) and state /= gameover and state /= pause and state /= powerup) then
			xpos <= xpos + 5;
		end if;
	end process;

	-- Determine if the ground should draw
	draw <= '1' when vpos >= ypos else '0';
	-- Get RGB data from rom
	RGB  <= rom_data(11 downto 0);

	process(rom_clock)
		-- Pixels drawn of the current line
		variable pixelcount         : integer range 0 to 63         := 0;
		-- The starting position of the current line
		variable last_line_start    : integer                       := 0;
		-- 
		variable prev_hpos_was_zero : std_logic                     := '0';

	begin
		if (rising_edge(rom_clock)) then
			-- Check if we are about to start drawing the ground
			if (hpos = 0 and prev_hpos_was_zero = '0') then
				prev_hpos_was_zero := '1';
				-- Set pixel count to current x offset
				pixelcount         := xpos;
				-- Check if we are drawing the top of the ground
				if (vpos = ypos) then
					-- Reset the line start
					last_line_start := 0;
				else
					-- Increment line start
					last_line_start := last_line_start + 64;
				end if;
			elsif (hpos /= 0) then
				prev_hpos_was_zero := '0';
			end if;
			-- Increment pixel count
			pixelcount  := pixelcount + 1;
			-- Set rom address
			rom_address <= std_logic_vector(to_unsigned(last_line_start + pixelcount, 13));
		end if;
	end process;
end architecture ground_arch;
