library ieee;
use ieee.std_logic_1164.all;

entity TenBitPRNG_Test is
end entity TenBitPRNG_Test;

architecture TenBitPRNG_TestArch of TenBitPRNG_Test is
	---------------------------------------------------------
	signal clock  : std_logic;
	signal reset  : std_logic;
	signal values : std_logic_vector(9 downto 0);
	---------------------------------------------------------
  component TenBitPRNG is
	 generic(
		  -- Starting state of each bit
	 	 iv : std_logic_vector(9 downto 0)
	 );
	 port(
		  clock, reset : in  std_logic;
		  output       : out std_logic_vector(9 downto 0)
    );
  end component TenBitPRNG;
---------------------------------------------------------
begin
	PRNG : TenBitPRNG generic map((others => '1')) port map(clock, reset, values);

  -- Clock Gen
  process
  begin
    clock <= '1', '0' after 1 ns;
    wait for 2 ns;
  end process;
  
  reset <= '0', '1' after 30 ns, '0' after 40 ns;

end architecture TenBitPRNG_TestArch;
