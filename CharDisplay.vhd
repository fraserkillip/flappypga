library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity CharDisplay is
	generic(
		-- Character to display
		char       : character;
		-- Position to draw character at
		x_position : INTEGER RANGE 0 TO 1280 := 0;
		y_position : INTEGER RANGE 0 TO 1024 := 0;
		-- Scale to draw character at
		scale      : integer range 1 to 4
	);
	port(
		clk         : in  std_logic;
		hpos        : IN  INTEGER RANGE 0 TO 1280 := 0;
		vpos        : IN  INTEGER RANGE 0 TO 1024 := 0;
		draw        : out std_logic;
		rom_address : out std_logic_vector(12 downto 0)
	);
end entity CharDisplay;

architecture CharDisplay_arch of CharDisplay is
	constant xpos : INTEGER RANGE 0 TO 1280 := x_position;
	constant ypos : INTEGER RANGE 0 TO 1024 := y_position;

	signal in_bounds : std_logic;
	-- Rom signals
	type switch_t is array (boolean) of integer;
	constant switch      : switch_t              := (true => character'pos(char) - 65, false => 26);
	constant char_offset : integer range 0 to 26 := switch(char /= ' ');
	constant rom_offset  : std_logic_vector(12 downto 0) :=  std_logic_vector(to_unsigned(char_offset * 224, 13));

begin

	-- Determine if the current draw position is within the bounds of this character
	in_bounds <= '1' when xpos <= hpos and xpos + 14 * scale > hpos and ypos <= vpos and ypos + 16 * scale > vpos else '0';

	process(clk)
		-- Starting pixel of the current line
		variable lineStart          : std_logic_vector(12 downto 0) := (others => '0');
		-- Number of pixels of the current line which have been drawn
		variable pixelcount         : integer range 0 to 14         := 0;
		-- Count of how many times the current pixel/row has been drawn
		variable scaler_x, scaler_y : integer range 1 to scale      := 1;
	begin
		if (rising_edge(clk)) then
			-- If we are at the top left location of the character..
			if (xpos = hpos and ypos = vpos) then
				-- Reset variables
				lineStart  := rom_offset;
				pixelcount := 0;
				scaler_x   := 1;
				scaler_y   := 1;
			end if;

			-- Set rom address
			rom_address <= lineStart + pixelcount;

			-- If we are at the start of a line (But not the top line)...
			if (xpos = hpos and ypos /= vpos) then
				-- If Y scaler has overflowed..
				if (scaler_y = scale) then
					-- Increment lineStart and reset y scaler
					lineStart := lineStart + 14;
					scaler_y  := 1;
				else
					-- Increment Y scaler
					scaler_y := scaler_y + 1;
				end if;
				-- Reset pixel count
				pixelcount := 0;
			end if;

			-- If we are in bounds and have drawn less than 14 pixels...
			if (in_bounds = '1' AND pixelcount < 14) then
				-- If X scaler has overflowed..
				if (scaler_x = scale) then
					-- Increment pixel count
					pixelcount := pixelcount + 1;
					-- Reset X scaler
					scaler_x   := 1;
				else
					-- Increment X scaler
					scaler_x := scaler_x + 1;
				end if;
			end if;
		end if;
	end process;

	-- Determine if this character should draw to the screen at the current position
	draw <= in_bounds;

end architecture CharDisplay_arch;
