library ieee;
use ieee.std_logic_1164.all;

entity RightShiftRegister is
	generic(
		iv : std_logic_vector
	);
	port(
		clock, reset, input : in  std_logic;
		output              : out std_logic_vector(iv'range)
	);
end entity RightShiftRegister;

architecture TenBitShiftRightRegisterArch of RightShiftRegister is
	---------------------------------------------------------
	signal values : std_logic_vector(iv'range) := iv;
	component DFlipFlop is
		generic(
			initial : std_logic
		);
		port(
			clock, reset, input : in  std_logic;
			output              : out std_logic
		);
	end component DFlipFlop;
---------------------------------------------------------
begin
	-- Generate DFlipFlip bits for shift register
	GEN_D : for I in 0 to iv'length - 1 generate
		FIRST_BIT : if I = 0 generate
			-- Map input to first bit
			DD0 : DFlipFlop generic map(iv(0)) port map(clock, reset, input, values(0));
		end generate FIRST_BIT;

		OTHER_BITS : if I > 0 generate
			-- Link output of previous bits to input
			DX : DFlipFlop generic map(iv(I)) port map(clock, reset, values(I - 1), values(I));
		end generate OTHER_BITS;

	end generate GEN_D;

	output <= values;
end architecture TenBitShiftRightRegisterArch;
