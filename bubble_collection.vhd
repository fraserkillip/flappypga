library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.definitions.all;

ENTITY BUBBLE_COLLECTION IS
	PORT(
		state     : in  game_state;
		clock10ms : IN  STD_LOGIC;
		hpos      : IN  INTEGER RANGE 0 TO 1280 := 0;
		vpos      : IN  INTEGER RANGE 0 TO 1024 := 0;
		RGB       : out std_logic_vector(11 downto 0);
		prng      : in  std_logic_vector(9 downto 0);
		draw      : out std_logic
	);
END BUBBLE_COLLECTION;

ARCHITECTURE BUBBLE_COLLECTION_ARCH OF BUBBLE_COLLECTION IS
	-- Number of bubbles to generate
	constant bubble_count : integer := 10;

	-- Draw signals for each bubble
	SIGNAL draw_vector : std_logic_vector(bubble_count - 1 downto 0) := (others => '0');
	---------------------------------------------------------
	COMPONENT BUBBLE IS
		generic(
			init_delay : in integer range 0 to 1024
		);
		port(
			state : in  game_state;
			clk   : in  std_logic;
			hpos  : in  integer range 0 to 1688 := 0;
			vpos  : in  integer range 0 to 1066 := 0;
			prng  : in  std_logic_vector(9 downto 0);
			draw  : out std_logic
		);
	END COMPONENT BUBBLE;
---------------------------------------------------------
BEGIN
	-- Generate bubble entities
	BUBBLES_GEN : for i in 0 to bubble_count - 1 generate
		BUBBLEX : BUBBLE GENERIC MAP(i * 1000 / bubble_count) -- Give each bubble a different starting delay
			PORT MAP(state, clock10ms, hpos, vpos, prng, draw_vector(i));
	end generate BUBBLES_GEN;

	-- Output the bubbles colour
	RGB <= x"3CF";

	-- Determine if any of the bubbles need to be drawn at the current pixel
	draw <= '1' when draw_vector /= (draw_vector'range => '0') else '0';

END BUBBLE_COLLECTION_ARCH; --mmm i like bubbles. buuuuuuuubbles. i mean, buuuuuuuuuuuuubbles. pop.