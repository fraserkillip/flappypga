library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity Hyperbeam is
	port(
		state                      : in  game_state;
		clk, rom_clock, beam_ready : in  std_logic;
		hpos                       : in  integer range 0 to 1280;
		vpos                       : in  integer range 0 to 1024;
		karp_x                     : in  integer range 0 to 1280;
		karp_y                     : in  integer range 0 to 1024;
		mouse_right                : in  std_logic;
		RGB                        : out std_logic_vector(11 downto 0);
		draw, beam_active          : out std_logic
	);
end entity Hyperbeam;

architecture Hyperbeam_arch of Hyperbeam is
	-- Rom signals
	SIGNAL rom_data    : STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL rom_address : STD_LOGIC_VECTOR(11 DOWNTO 0);

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(11 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
		);
	END COMPONENT;

	-- Speed that the beam moves across the screen at
	constant beam_speed : integer range 1 to 10   := 10;
	-- Current position of the beam
	signal xpos         : integer range 0 to 1280;
	signal ypos         : integer range 0 to 1024;
	-- Current width of the beam
	signal beam_width   : integer range 0 to 1280 := 0;
	signal in_bounds    : std_logic;

begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "beam.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 4096,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 12,
			width_a                => 13,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => rom_clock,
			address_a => rom_address,
			q_a       => rom_data
		);

	-- Update beam state
	process(clk)
		variable step : std_logic_vector(4 downto 0) := (others => '0');
	begin
		if (rising_edge(clk)) then
			-- If in a playable state....
			if (state = playing or state = tutorial or state = powerup) then
				-- If beam exits...
				if (beam_width > 0) then
					-- If the beam will not move past the edge of the screen..
					if (xpos + beam_width >= 1280 - beam_speed) then
						-- Step.
						step := step + 1;
						-- If step overflows..
						if (step = 0) then
							-- Reset beam
							beam_width  <= 0;
							beam_active <= '0';
						end if;
					else
						-- Increment beam width
						beam_width <= beam_width + beam_speed;
					end if;
				elsif (mouse_right = '1' and beam_ready = '1') then -- If right mouse button is clicked
					-- Activate beam
					beam_width  <= 1;
					beam_active <= '1';
					-- Reset step
					step        := (4 => '1', others => '0');
				end if;
			else
				-- (If in unplayable state) disable beam
				beam_width <= 0;
				beam_active <= '0';
			end if;
		end if;
	end process;

	-- Set position of the beam (Follows karps location)
	xpos      <= karp_x + 64;
	ypos      <= karp_y;
	-- Determine if the draw position is in the beams bounds
	in_bounds <= '1' when ypos < vpos and ypos + 64 > vpos and xpos < hpos and xpos + beam_width > hpos else '0';
	-- Determine if the beam should draw to the current draw position
	draw      <= '1' when in_bounds = '1' and rom_data(12) = '1' else '0';
	-- Output beam RGB
	RGB       <= rom_data(11 downto 0);

	-- Update rom address
	process(rom_clock)
		-- Current rom address
		variable position   : std_logic_vector(11 downto 0) := (others => '0');
		-- Number of pixels of the current line which have been drawn
		variable pixelcount : integer range 0 to 62         := 0;
	begin
		if (rising_edge(rom_clock)) then
			-- If at the top left of the beam...
			if (xpos = hpos and ypos = vpos) then
				-- Reset position
				position := (others => '0');
			end if;
			-- Set rom address
			rom_address <= position;

			-- If we are at the start of a line...
			if (pixelcount >= 62 AND xpos = hpos) then
				-- Reset pixel count
				pixelcount := 0;
				-- Increment position to start of current line
				position   := position + 2;
			end if;

			-- If we are in bounds and pixel count is less than 62...
			-- Note: pixel 62 of each line is drawn repeatedly till the edge of the beam
			if (in_bounds = '1' AND pixelcount < 62) then
				-- Increment position
				position   := position + 1;
				-- Increment pixel counter
				pixelcount := pixelcount + 1;
			end if;
		end if;
	end process;
end architecture Hyperbeam_arch;
