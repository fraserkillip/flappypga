library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity Pipe is
	generic(
		initial_position : integer range 1280 to 2560
	);
	port(
		state                   : in  game_state;
		clk, rom_clock, disable : in  std_logic := '0';
		karp_pos, hpos          : in  integer range 0 to 1280;
		vpos                    : in  integer range 0 to 1024;
		prng                    : in  std_logic_vector(9 downto 0);
		rom_address             : out std_logic_vector(11 DOWNTO 0);
		draw, score             : out std_logic
	);
end entity Pipe;

architecture Pipe_Arch of Pipe is
	-- Width of gap in pipes
	constant gap_width : integer := 250;
	-- Width of pipes
	constant width     : integer := 64;
	-- Speed of pipes
	constant x_speed   : integer := 5;

	-- X Position of the pipe
	signal xpos : integer range -width to 2560 := initial_position;
	-- Y Position of the top of the bottom part of the pipe
	signal ypos : integer range 0 to 1023      := 512 - gap_width;

	signal in_bounds, is_disabled : std_logic := '0';

begin
	-- Update pipes location
	process(clk, disable, in_bounds, xpos)
	begin
		-- Check if the disable ability is active when the pipe is drawing..
		if (disable = '1' and in_bounds = '1') then
			-- Disable the pipe from drawing if it can be seen on screen
			if (xpos < 1280) then
				is_disabled <= disable;
			end if;
		elsif (rising_edge(clk)) then
			-- Check if we are in a playable state
			if (state = playing or state = tutorial) then
				-- Check if the pipe has left the left edge of the screen
				if (xpos <= -width - 32) then
					-- Reset pipe to the right edge of the screen
					xpos        <= 1280;
					ypos        <= to_integer(unsigned(prng(8 downto 0))) + 127;
					-- Reset disabled flag
					is_disabled <= '0';
				else
					-- Move the pipe left
					xpos <= xpos - x_speed;
				end if;
			elsif (state = idle) then   -- Reset pipe in idle state
				xpos        <= initial_position;
				is_disabled <= '0';
				-- Set Random y position
				ypos        <= to_integer(unsigned(prng(8 downto 0))) + 127;
			end if;
		end if;
	end process;

	-- Determine if the current draw position is within the bounds of this pipe
	in_bounds <= '1' when xpos <= hpos and xpos + width > hpos and (ypos >= vpos or ypos + gap_width < vpos) and vpos < 960 else '0';
	-- Determine if this pipe should be allowed to drawn
	draw      <= in_bounds and not is_disabled;

	-- Determine if the karp has passed this pipe
	score <= '1' when karp_pos > (xpos + width) and karp_pos < (xpos + width + 6) else '0';

	-- Sets rom address
	process(rom_clock, in_bounds)
		-- The starting position in the rom for the current line
		variable line_start  : integer range 0 to 4095 := 0;
		-- Number of pixels drawn on the current line
		variable pixel_count : integer range 0 to 64   := 0;
	begin
		-- Only update when in bounds
		if (rising_edge(rom_clock) and in_bounds = '1') then
			-- If we are at the start of the first line...
			if (vpos = 0) then
				-- Reset variables
				line_start := 0;
				if (xpos < 0) then
					-- Start pixel count at the correct position
					pixel_count := 63 - xpos;
				elsif (xpos = hpos) then
					pixel_count := 0;
				end if;
			end if;

			-- Check if we are at the start of a new line
			if (xpos = hpos and vpos /= 0) then
				-- Increment line start by width of pipe image
				line_start  := line_start + 64;
				-- Reset pixel count
				pixel_count := 0;
			elsif (xpos < 0 and hpos = 0) then -- Check if the pipe is off screen...
				-- Increment line start by width of pipe image
				line_start  := line_start + 64;
				-- Start pixel count at the correct position
				pixel_count := 63 - xpos;
			end if;

			-- Set rom address
			rom_address <= std_logic_vector(to_unsigned(line_start, 12)) + pixel_count;

			-- While in pipe bounds...
			if (in_bounds = '1') then
				-- Increment pixel count
				pixel_count := pixel_count + 1;
			end if;
		end if;
	end process;
end architecture Pipe_Arch;
