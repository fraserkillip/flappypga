library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity background is
	port(
		hpos : in INTEGER range 0 to 1688 := 0;
		vpos : in INTEGER range 0 to 1066 := 0;
		RGB : out std_logic_vector(11 downto 0)
	);
end background;

architecture background_arch of background is

BEGIN
	RGB <=  -- OCEAN - Determine background colour at current draw position
			x"0AE" when vpos <= 128 else -- slowly
			x"09D" when vpos <= 256 else -- the
			x"08C" when vpos <= 384 else -- ocean
			x"07B" when vpos <= 512 else -- gets
			x"06A" when vpos <= 640 else -- darker
			x"059" when vpos <= 768 else
			x"048";
end background_arch;