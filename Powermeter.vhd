library ieee;
use ieee.std_logic_1164.all;

use work.definitions.all;

entity Powermeter is
	port(
		clock, pixel_clock, powerup_activate : in  std_logic;
		state                                : game_state;
		hpos                                 : in  integer range 0 to 1280;
		vpos                                 : in  integer range 0 to 1024;
		powerup_ready, draw                  : out std_logic
	);
end entity Powermeter;

architecture Powermeter_arch of Powermeter is
	-- Power meter component draw signals
	signal left, right, top, bottom, bar : std_logic                := '0';
	-- Positioning of the power meter
	constant xpos                        : integer range 0 to 1280  := 20;
	constant ypos                        : integer range 0 to 1024  := 60;
	constant width                       : integer                  := 235;
	constant height                      : integer                  := 25;
	constant border_width                : integer                  := 2;
	signal progress                      : integer range 0 to width := 0;
begin
	-- Increments progress of power meter
	process(clock, powerup_activate, state)
		variable clock_div : integer range 0 to 3 := 0;
	begin
		-- Check if the powerup has been used or we are not in a playable state...
		if (powerup_activate = '1' or (state /= playing and state /= tutorial)) then
			-- Reset progress meter
			progress <= 0;
		elsif (rising_edge(clock)) then
			-- Increment progress while we have not filled the meter
			if (clock_div = 3 and progress /= width) then
				progress <= progress + 1;
			end if;
			clock_div := clock_div + 1;
		end if;
	end process;

	-- Determine components of the power meter need to draw on the current pixel
	left   <= '1' when xpos <= hpos and xpos + border_width > hpos and ypos <= vpos and ypos + height > vpos else '0';
	right  <= '1' when xpos + width <= hpos and xpos + width + border_width > hpos and ypos <= vpos and ypos + height + border_width> vpos else '0';
	top    <= '1' when xpos <= hpos and xpos + width > hpos and ypos <= vpos and ypos + border_width > vpos else '0';
	bottom <= '1' when xpos <= hpos and xpos + width > hpos and ypos + height <= vpos and ypos + height + border_width > vpos else '0';

	bar <= '1' when xpos <= hpos and xpos + progress > hpos and xpos + width > hpos and ypos <= vpos and ypos + height > vpos else '0';

	-- Determine if the power meter is full
	powerup_ready <= '1' when progress = width else '0';

	-- Determine if any part of the progress meter needs to draw
	draw <= left or right or top or bottom or bar;

end architecture Powermeter_arch;
