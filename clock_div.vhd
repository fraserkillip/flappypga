library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity clock10ms_div is
	port(
		clk50                  : in  std_logic;
		clk10ms_out, clk25_out : out std_logic
	);
end clock10ms_div;

architecture clock10ms_div_arch of clock10ms_div is
	-- Signals for generated clocks
	signal clk10ms, clock25 : std_logic := '0';
begin
	-- Output clocks
	clk10ms_out <= clk10ms;
	clk25_out <= clock25;

	-- Generate 10ms clock
	process(clk50)
		variable delay : integer range 0 to 500000 := 0;
	begin
		if (rising_edge(clk50)) then
			-- If we reach the delay..
			if (delay = 500000) then
				-- ...Invert clock signal
				clk10ms <= not clk10ms;
				-- ...Reset delay
				delay   := 0;
			else
				-- ... else increment delay
				delay := delay + 1;
			end if;
		end if;
	end process;

	process(clk50)
		variable delay : integer range 0 to 1;
	begin
		if (rising_edge(clk50)) then
			-- If we reach the delay...
			if (delay = 1) then
				-- ...Invert clock signal
				clock25 <= not clock25;
				-- ...Reset delay
				delay     := 0;
			else
				-- ... else increment delay
				delay := delay + 1;
			end if;
		end if;
	end process;
end clock10ms_div_arch;