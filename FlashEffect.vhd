library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity FlashEffect is
	port(
		clock, collide : in  std_logic;
		RGB_in         : in  std_logic_vector(11 downto 0);
		RGB_out        : out std_logic_vector(11 downto 0)
	);
end entity FlashEffect;

architecture FlashEffect_arch of FlashEffect is
	-- The signal to add to the RGB that makes it 'flash'
	signal white_flash : std_logic_vector(11 downto 0);
begin

	-- Add the flash to the rgb signal
	add_value : process(RGB_in, white_flash) is
		variable r, g, b : std_logic_vector(4 downto 0);
	begin
		-- We need to cap the addition to x"F" so we include an overflow bit
		r := ('0' & RGB_in(11 downto 8)) + white_flash(11 downto 8);
		g := ('0' & RGB_in(7 downto 4)) + white_flash(7 downto 4);
		b := ('0' & RGB_in(3 downto 0)) + white_flash(3 downto 0);

		-- Cap the signal to x"F"
		if r > x"F" then
			r := '0' & x"F";
		end if;
		if g > x"F" then
			g := '0' & x"F";
		end if;
		if b > x"F" then
			b := '0' & x"F";
		end if;

		-- Reassign the output
		RGB_out <= r(3 downto 0) & g(3 downto 0) & b(3 downto 0);
	end process add_value;

	start_flash : process(clock) is
		variable clock_div    : integer range 0 to 2 := 0;
		-- Used to spoof a rising edge
		variable last_collide : std_logic            := '0';
	begin
		if (rising_edge(clock)) then
			-- Whenever we collide with something
			if (collide = '1' and last_collide = '0') then
				white_flash <= x"bbb";
			end if;
			if (clock_div = 2) then
				-- If the flash is greater than 000 then subtract 111 until it's 000
				if (white_flash /= x"000") then
					white_flash <= white_flash - x"111";
				end if;
			end if;
			-- Assign the last value of collide that we saw
			last_collide := collide;
			clock_div    := clock_div + 1;
		end if;
	end process start_flash;

end architecture FlashEffect_arch;
