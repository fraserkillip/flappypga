library ieee;
use ieee.std_logic_1164.all;

entity StringDisplay is
	generic(
		-- Characters to display
		message    : string;
		-- X/Y Positions to display the string
		x_position : INTEGER RANGE 0 TO 1280 := 0;
		y_position : INTEGER RANGE 0 TO 1024 := 0;
		-- Scale of the string to display
		scale      : integer range 1 to 4
	);
	port(
		clk  : in  std_logic;
		hpos : IN  INTEGER RANGE 0 TO 1280 := 0;
		vpos : IN  INTEGER RANGE 0 TO 1024 := 0;
		draw : out std_logic
	);
end entity StringDisplay;

architecture StringDisplay_arch of StringDisplay is
	component CharDisplay is
		generic(
			char       : character;
			x_position : INTEGER RANGE 0 TO 1280 := 0;
			y_position : INTEGER RANGE 0 TO 1024 := 0;
			scale      : integer range 1 to 4
		);
		port(
			clk  : in  std_logic;
			hpos : IN  INTEGER RANGE 0 TO 1280 := 0;
			vpos : IN  INTEGER RANGE 0 TO 1024 := 0;
			draw : out std_logic;
			rom_address : out std_logic_vector(12 downto 0)
		);
	end component CharDisplay;

	-- Determine the spacing between each character
	constant dx : integer range 15 * scale to 15 * scale := 15 * scale;
	-- Draw vector for each character
	signal draw_vector        : std_logic_vector(message'length - 1 downto 0);
	signal char_rom_addresses : std_logic_vector(13*message'length - 1 downto 0);
	
	-- Rom signals
	signal rom_address        : std_logic_vector(12 downto 0);
	signal rom_data           : std_logic_vector(0 downto 0);
	
	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(12 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
		);
	END COMPONENT;

begin
		altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "chars.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 6048,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 13,
			width_a                => 1,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => clk,
			address_a => rom_address,
			q_a       => rom_data
		);
		
	-- Generate each character
	chars : for i in 0 to message'length - 1 generate
		char : CharDisplay
			-- Map the current character to display and position
			generic map(char        => message(i + 1),
				        x_position  => x_position + i * dx,
				        y_position  => y_position,
				        scale       => scale)
			port map(clk  => clk,
				     hpos => hpos,
				     vpos => vpos,
				     draw => draw_vector(i),
				     rom_address => char_rom_addresses(13*(i+1)-1 downto 13 * i));
	end generate;

	-- Determine if any character wants to draw at the current position
	draw <= '1' and rom_data(0) when draw_vector /= (draw_vector'range => '0') else '0';
	
		-- Fetch rom data
	process(clk)
	begin
		if (rising_edge(clk)) then
			-- For each character...
			for I in 0 to message'length - 1 loop
				-- If pipe is drawing...
				if (draw_vector(i) = '1' ) then
					-- Set rom address character wants
					rom_address <= char_rom_addresses(13 * (i+1) - 1 downto 13 * i);
					exit;
				end if;
			end loop;
		end if;
	end process;

end architecture StringDisplay_arch;
