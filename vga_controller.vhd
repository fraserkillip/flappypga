library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity VGA_CONTROLLER is
	port(
		clock108              : in  std_logic;
		hsync, vsync, porched : out std_logic;
		hpos_out              : out integer range 0 to 1688 := 0;
		vpos_out              : out integer range 0 to 1066 := 0;
		RGB                   : out std_logic_vector(11 downto 0);
		RGBin                 : in  std_logic_vector(11 downto 0)
	);
end VGA_CONTROLLER;

architecture VGA_CONTROLLER_ARCH of VGA_CONTROLLER is
begin
	process(clock108) IS
		variable hpos : integer range 0 to 1688 := 0;
		variable vpos : integer range 0 to 1066 := 0;
	begin
		-- Move the draw point
		if (rising_edge(clock108)) then
			if (hpos < 1688) then
				hpos := hpos + 1;
			else
				hpos := 0;
				if (vpos < 1066) then
					vpos := vpos + 1;
				else
					vpos := 0;
				end if;
			end if;

			-- Horizontal sync
			if (hpos > 48 and hpos < 160) then
				hsync <= '0';
			else
				hsync <= '1';
			end if;

			-- Vertical sync
			if (vpos > 0 and vpos < 4) then
				vsync <= '0';
			else
				vsync <= '1';
			end if;

			if ((hpos > 0 and hpos < 408) or (vpos > 0 and vpos < 42)) then
				RGB     <= x"000";      -- RGB has to be zero during porch
				porched <= '1';
			else
				porched <= '0';
				RGB     <= RGBin;
			end if;
		end if;

		-- 
		if (hpos > 407) then
			hpos_out <= (hpos - 408);
		else
			hpos_out <= 0;
		end if;
		if (vpos > 41) then
			vpos_out <= (vpos - 42);
		else
			vpos_out <= 0;
		end if;
	end process;
end VGA_CONTROLLER_ARCH;