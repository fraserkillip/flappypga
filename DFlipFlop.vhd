library ieee;
use ieee.std_logic_1164.all;

-- Simple D Flip Flop with Asyncronous reset
entity DFlipFlop is
	generic (
		-- Initial state of bit
		initial : std_logic
	);
	port (
		clock, reset, input: in std_logic;
		output: out std_logic := initial
	);
end entity DFlipFlop;

architecture DFlipFlopArch of DFlipFlop is
begin
	process(clock, reset)
	begin
		if (reset = '1') then
			-- Reset state (async)
			output <= initial;
		elsif (rising_edge(clock)) then
			-- Update state
			output <= input;
		end if;
	end process;
end architecture DFlipFlopArch;
