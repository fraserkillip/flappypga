library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity NumDisplay is
	generic(
		-- Postion to draw the number
		x_position : INTEGER RANGE 0 TO 1280 := 0;
		y_position : INTEGER RANGE 0 TO 1024 := 0;
		-- Scale to draw the number
		scale      : integer range 1 to 4
	);
	port(
		clk  : in  std_logic;
		hpos : IN  INTEGER RANGE 0 TO 1280 := 0;
		vpos : IN  INTEGER RANGE 0 TO 1024 := 0;
		num  : integer range 0 to 9;
		draw : out std_logic
	);
end entity NumDisplay;

architecture NumDisplay_arch of NumDisplay is
	-- Position of the number
	constant xpos : INTEGER RANGE 0 TO 1280 := x_position;
	constant ypos : INTEGER RANGE 0 TO 1024 := y_position;

	signal in_bounds   : std_logic;
	-- Rom signals
	SIGNAL rom_data    : STD_LOGIC_VECTOR(0 DOWNTO 0);
	SIGNAL rom_address : STD_LOGIC_VECTOR(11 DOWNTO 0);

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(11 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
		);
	END COMPONENT;
begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "chars/nums.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 2240,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 12,
			width_a                => 1,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => clk,
			address_a => rom_address,
			q_a       => rom_data
		);

	-- Determine if the current draw position is in the numbers bounds
	in_bounds <= '1' when xpos <= hpos and xpos + 14 * scale > hpos and ypos <= vpos and ypos + 16 * scale > vpos else '0';

	-- Updates rom address
	process(clk)
		-- Starting pixel of the current line
		variable lineStart          : integer range 0 to 2240  := 0;
		-- Number of pixels of the current line which have been drawn
		variable pixelcount         : integer range 0 to 14    := 0;
		-- Count of how many times the current pixel/row has been drawn
		variable scaler_x, scaler_y : integer range 1 to scale := 1;
	begin
		if (rising_edge(clk)) then
			-- If drawing at the top left of the number...
			if (xpos = hpos and ypos = vpos) then
				-- Reset variables
				lineStart  := 224 * num; -- Line start = {bits per number} * {number to draw}
				pixelcount := 0;
				scaler_x   := 1;
				scaler_y   := 1;
			end if;

			-- Set rom address
			rom_address <= std_logic_vector(to_unsigned(lineStart, 12)) + pixelcount;

			-- If we are at the start of a line (But not the top line)...
			if (xpos = hpos and ypos /= vpos) then
				-- If Y scaler has overflowed..
				if (scaler_y = scale) then
					-- Increment lineStart and reset y scaler
					lineStart := lineStart + 14;
					scaler_y  := 1;
				else
					-- Increment Y scaler
					scaler_y := scaler_y + 1;
				end if;
				-- Reset pixel count
				pixelcount := 0;
			end if;

			-- If we are in bounds and have drawn less than 14 pixels...
			if (in_bounds = '1' AND pixelcount < 14) then
				-- If X scaler has overflowed..
				if (scaler_x = scale) then
					-- Increment pixel count
					pixelcount := pixelcount + 1;
					-- Reset X scaler
					scaler_x   := 1;
				else
					-- Increment X scaler
					scaler_x := scaler_x + 1;
				end if;
			end if;
		end if;
	end process;

	-- Determine if we are drawing the number at the current draw location
	draw <= in_bounds and rom_data(0);

end architecture NumDisplay_arch;
