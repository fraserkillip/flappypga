library ieee;
use ieee.std_logic_1164.all;

use work.definitions.all;

entity pipe_collection is
	port(
		state                     : in  game_state;
		clk, rom_clock, beam_draw : in  std_logic;
		karp_pos, hpos            : in  integer range 0 to 1280;
		vpos                      : in  integer range 0 to 1024;
		RGB                       : out std_logic_vector(11 downto 0);
		draw, score               : out std_logic
	);
end entity pipe_collection;

architecture pipe_collection_arch of pipe_collection is
	-- Number of pipes to generate
	constant pipe_count : integer := 3;

	-- Rom signals
	SIGNAL rom_data    : STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL rom_address : STD_LOGIC_VECTOR(11 DOWNTO 0);

	-- Rom addresses for each pipe
	signal pipe_r_addresses : std_logic_vector(pipe_count * 12 - 1 downto 0);

	-- Draw signals for each pipe
	signal draw_vector  : std_logic_vector(pipe_count - 1 downto 0);
	-- Score signals for each pipe
	signal score_vector : std_logic_vector(pipe_count - 1 downto 0);

	-- PRNG output for pipes
	signal pipe_prng_output : std_logic_vector(9 downto 0);
	component TenBitPRNG is
		generic(
			iv : std_logic_vector(9 downto 0)
		);
		port(
			clock, reset : in  std_logic;
			output       : out std_logic_vector(9 downto 0)
		);
	end component TenBitPRNG;

	component Pipe is
		generic(
			initial_position : integer range 1280 to 2560
		);
		port(
			state                   : in  game_state;
			clk, rom_clock, disable : in  std_logic;
			karp_pos, hpos          : in  integer range 0 to 1280;
			vpos                    : in  integer range 0 to 1024;
			prng                    : in  std_logic_vector(9 downto 0);
			rom_address             : out std_logic_vector(11 DOWNTO 0);
			draw, score             : out std_logic
		);
	end component Pipe;

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(11 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
		);
	END COMPONENT;
begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "kelp.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 4096,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 12,
			width_a                => 13,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => rom_clock,
			address_a => rom_address,
			q_a       => rom_data
		);

	-- Create PRNG for pipes
	PIPE_PRNG : component TenBitPRNG generic map("1011101101") port map(clock => clk, reset => '0', output => pipe_prng_output);
	-- Generate each pipe
	PIPE_GEN : for i in 0 to pipe_count - 1 generate
		PIPEX : component Pipe
			-- Try to evenly space each pipe
			generic map(initial_position => 1280 + 1280 / pipe_count * i)
			port map(state, clk, rom_clock, beam_draw, karp_pos, hpos, vpos, pipe_prng_output, pipe_r_addresses(11 + 12 * i downto 12 * i), draw_vector(i), score_vector(i));
	end generate;

	-- Determine if any pipes want to draw
	draw <= rom_data(12) when draw_vector /= (draw_vector'range => '0') else '0';
	-- Output RGB for pipe
	RGB  <= rom_data(11 downto 0);

	-- Determine if any pipes score signals are active
	score <= '1' when score_vector /= (score_vector'range => '0') else '0';

	-- Fetch rom data
	process(rom_clock)
	begin
		if (rising_edge(rom_clock)) then
			-- For each pipe...
			for I in 0 to pipe_count - 1 loop
				-- If pipe is drawing...
				if (draw_vector(i) = '1' ) then
					-- Set rom address pipe wants
					rom_address <= pipe_r_addresses(11 + 12 * i downto 12 * i);
					exit;
				end if;
			end loop;
		end if;
	end process;
end architecture pipe_collection_arch;
