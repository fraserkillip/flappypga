library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity MifSoundPlayer is
	generic(
		rom_name      : string;
		num_words     : integer;
		address_width : integer
	);
	port(
		clock, sample_clock : in  std_logic;
		play, loop_play     : in  std_logic := '0';
		sound               : out std_logic);
end entity MifSoundPlayer;

architecture MifSoundPlayer_arch of MifSoundPlayer is
	signal playing : std_logic;

	signal pwm_out : std_logic;

	signal rom_address : std_logic_vector(address_width - 1 downto 0);
	signal rom_data    : std_logic_vector(7 downto 0);

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(address_width - 1 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
		);
	END COMPONENT;

	signal pwm_reset, pwm_enable : std_logic := '0';
	component pwm is
		generic(
			sys_clk         : integer := 50_000_000; --system clock frequency in Hz
			pwm_freq        : integer := 100_000; --PWM switching frequency in Hz
			bits_resolution : integer := 8); --bits of resolution setting the duty cycle
		port(
			clk     : in  std_logic;    --system clock
			reset_n : in  std_logic;    --asynchronous reset
			enable  : in  std_logic;    --latches in new duty cycle
			value   : in  std_logic_vector(bits_resolution - 1 downto 0); --duty cycle
			pwm_out : out std_logic);   --pwm outputs
	end component pwm;
begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => rom_name,
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => num_words,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => address_width,
			width_a                => 8,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => clock,
			address_a => rom_address,
			q_a       => rom_data
		);

	pwm_component : pwm
		generic map(pwm_freq => 500_000)
		port map(clk     => clock,
			     reset_n => '1',
			     enable  => '1',
			     value   => rom_data,
			     pwm_out => pwm_out);

	sound  <= pwm_out;

	process(sample_clock)
	begin
		if (rising_edge(sample_clock)) then
			if (play = '1' or loop_play = '1') then
				playing <= '1';
			end if;
			if (playing = '1') then
				if (rom_address >= num_words) then
					rom_address <= (others => '0');
					if (loop_play = '0') then
						playing <= '0';
					end if;
				else
					rom_address <= rom_address + 1;
				end if;
			end if;
		end if;
	end process;

end architecture MifSoundPlayer_arch;
