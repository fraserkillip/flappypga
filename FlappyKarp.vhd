library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

use work.definitions.all;

entity FlappyKarp is
	port(
		clock50, mouseReset   : in    std_logic;
		VGA_hs, VGA_vs, sound : out   std_logic;
		VGA_r, VGA_g, VGA_b   : out   std_logic_vector(3 downto 0);
		mouse_data            : inout std_logic;
		mouse_clk             : inout std_logic;
		lights                : out   std_logic_vector(9 downto 0) := "1010101010";

		dip_tutorialPlay      : in    std_logic; -- 0
		dip_infinitePowerup   : in    std_logic; -- 1
		btn_reset             : in    std_logic
	);
end FlappyKarp;

architecture FlappyKarp_arch of FlappyKarp is
	signal vga_rgb          : std_logic_vector(11 downto 0) := "100110011001";
	signal VGA_RGB_OUT      : std_logic_vector(11 downto 0) := (others => '0');
	signal hpos             : integer range 0 to 1688       := 0;
	signal vpos             : integer range 0 to 1066       := 0;
	signal clock108, reset  : std_logic                     := 'X';
	signal collision        : std_logic                     := '0';
	signal score            : scores                        := (others => 0);
	signal state            : game_state                    := idle;
	signal startedInPlaying : std_logic                     := '1';
	---------------------------------------------------------
	component clock108pll is
		port(
			clk_in_clk  : in  std_logic := 'X'; -- clk
			reset_reset : in  std_logic := 'X'; -- reset
			clk_out_clk : out std_logic -- clk
		);
	end component clock108pll;
	---------------------------------------------------------
	signal clock10ms, clock25 : std_logic := '0';

	component clock10ms_div is
		port(
			clk50                  : in  std_logic;
			clk10ms_out, clk25_out : out std_logic
		);
	end component clock10ms_div;
	---------------------------------------------------------
	signal vsync, porched : std_logic;
	component vga_controller is
		port(
			clock108              : in  std_logic;
			hsync, vsync, porched : out std_logic;
			hpos_out              : out integer range 0 to 1688 := 0;
			vpos_out              : out integer range 0 to 1066 := 0;
			RGB                   : out std_logic_vector(11 downto 0);
			RGBin                 : in  std_logic_vector(11 downto 0)
		);
	end component vga_controller;
	---------------------------------------------------------
	signal bg_rgb : std_logic_vector(11 downto 0);

	component background is
		port(
			hpos : in  integer range 0 to 1688;
			vpos : in  integer range 0 to 1066;
			RGB  : out std_logic_vector(11 downto 0)
		);
	end component background;
	---------------------------------------------------------
	signal bubble_rgb  : std_logic_vector(11 downto 0);
	signal bubble_draw : std_logic;

	component bubble_collection is
		PORT(
			state     : in  game_state;
			clock10ms : IN  STD_LOGIC;
			hpos      : IN  INTEGER RANGE 0 TO 1280 := 0;
			vpos      : IN  INTEGER RANGE 0 TO 1024 := 0;
			RGB       : out std_logic_vector(11 downto 0);
			prng      : in  std_logic_vector(9 downto 0);
			draw      : out std_logic
		);
	end component bubble_collection;
	---------------------------------------------------------
	signal karp_rgb             : std_logic_vector(11 downto 0);
	signal karp_draw            : std_logic;
	signal karp_xpos, karp_ypos : integer range 0 to 1280;

	component Karp is
		port(
			state                      : in  game_state;
			clk, rom_clock, mouse_left : in  std_logic;
			hpos                       : in  integer range 0 to 1280;
			vpos                       : in  integer range 0 to 1024;
			xpos_out, ypos_out         : out integer range 0 to 1280;
			RGB                        : out std_logic_vector(11 downto 0);
			draw                       : out std_logic
		);
	end component Karp;
	---------------------------------------------------------
	signal hyperbeam_rgb               : std_logic_vector(11 downto 0);
	signal hyperbeam_draw, beam_active : std_logic := '0';

	component Hyperbeam is
		port(
			state                      : in  game_state;
			clk, rom_clock, beam_ready : in  std_logic;
			hpos                       : in  integer range 0 to 1280;
			vpos                       : in  integer range 0 to 1024;
			karp_x                     : in  integer range 0 to 1280;
			karp_y                     : in  integer range 0 to 1024;
			mouse_right                : in  std_logic;
			RGB                        : out std_logic_vector(11 downto 0);
			draw, beam_active          : out std_logic
		);
	end component Hyperbeam;
	---------------------------------------------------------
	signal PRNG_reset  : std_logic;
	signal PRNG_output : std_logic_vector(9 downto 0);
	component TenBitPRNG is
		generic(
			iv : std_logic_vector(9 downto 0)
		);
		port(
			clock, reset : in  std_logic;
			output       : out std_logic_vector(9 downto 0)
		);
	end component TenBitPRNG;
	---------------------------------------------------------
	signal left_button, right_button, mouse_reset : std_logic;
	signal mouse_cursor_row                       : integer range 0 to 1280;
	signal mouse_cursor_column                    : integer range 0 to 1024;
	component MOUSE is
		port(clock_25Mhz, reset               : in    std_logic;
			 signal mouse_data                : inout std_logic;
			 signal mouse_clk                 : inout std_logic;
			 signal left_button, right_button : out   std_logic;
			 signal mouse_cursor_row          : out   integer range 0 to 1280;
			 signal mouse_cursor_column       : out   integer range 0 to 1024);
	end component MOUSE;
	---------------------------------------------------------
	signal pipe_draw, score_trigger : std_logic;
	signal pipe_rgb                 : std_logic_vector(11 downto 0);

	component pipe_collection is
		port(
			state                     : in  game_state;
			clk, rom_clock, beam_draw : in  std_logic;
			karp_pos, hpos            : in  integer range 0 to 1280;
			vpos                      : in  integer range 0 to 1024;
			RGB                       : out std_logic_vector(11 downto 0);
			draw, score               : out std_logic
		);
	end component pipe_collection;
	---------------------------------------------------------
	signal ground_draw : std_logic;
	signal ground_rgb  : std_logic_vector(11 downto 0);

	component ground is
		port(
			state          : in  game_state;
			clk, rom_clock : in  std_logic;
			hpos           : in  integer range 0 to 1280;
			vpos           : in  integer range 0 to 1024;
			RGB            : out std_logic_vector(11 downto 0);
			draw           : out std_logic
		);
	end component ground;
	---------------------------------------------------------
	signal scoreDisplayPlaying_draw, scoreTextPlaying_draw   : std_logic;
	signal scoreDisplayGameover_draw, scoreTextGameover_draw : std_logic;

	component ScoreDisplay is
		generic(
			xpos  : integer range 0 to 1280;
			ypos  : integer range 0 to 1024;
			size  : integer range 0 to 3;
			scale : integer range 1 to 4
		);
		port(
			clk   : in  std_logic;
			hpos  : in  integer range 0 to 1280;
			vpos  : in  integer range 0 to 1024;
			score : in  scores;
			draw  : out std_logic
		);
	end component ScoreDisplay;
	---------------------------------------------------------
	signal title_draw, ctp_draw, go_draw, tut_draw : std_logic;

	constant text_colour : std_logic_vector(11 downto 0) := x"DA4";

	component StringDisplay is
		generic(
			message    : string;
			x_position : INTEGER RANGE 0 TO 1280 := 0;
			y_position : INTEGER RANGE 0 TO 1024 := 0;
			scale      : integer range 1 to 4
		);
		port(
			clk  : in  std_logic;
			hpos : IN  INTEGER RANGE 0 TO 1280 := 0;
			vpos : IN  INTEGER RANGE 0 TO 1024 := 0;
			draw : out std_logic
		);
	end component StringDisplay;
	---------------------------------------------------------
	signal flash_rgb : std_logic_vector(11 downto 0) := x"DA4";
	component FlashEffect is
		port(
			clock, collide : in  std_logic;
			RGB_in         : in  std_logic_vector(11 downto 0);
			RGB_out        : out std_logic_vector(11 downto 0)
		);
	end component FlashEffect;
	---------------------------------------------------------
	signal darken_rgb : std_logic_vector(11 downto 0) := x"DA4";
	component HyperbeamDarken is
		port(
			beam_active, beam_draw : in  std_logic;
			RGB_in                 : in  std_logic_vector(11 downto 0);
			RGB_out                : out std_logic_vector(11 downto 0)
		);
	end component HyperbeamDarken;
	---------------------------------------------------------
	signal powerup_ready, pm_draw : std_logic := '0';
	component Powermeter is
		port(
			clock, pixel_clock, powerup_activate : in  std_logic;
			state                                : game_state;
			hpos                                 : in  integer range 0 to 1280;
			vpos                                 : in  integer range 0 to 1024;
			powerup_ready, draw                  : out std_logic
		);
	end component Powermeter;
	---------------------------------------------------------
	signal sound_out : std_logic;
	component SoundController is
		port(
			clock50, score_trigger : in  std_logic;
			sound                  : out std_logic
		);
	end component SoundController;
---------------------------------------------------------
begin
	reset       <= '0';
	mouse_reset <= not mouseReset;
	PRNG_reset  <= '0';

	-- Generates the 108MHz pixel clock (from PLL)
	CLOCK108_GEN : clock108pll port map(clock50, reset, clock108);
	-- Generates the 10ms game clock
	CLOCK10_GEN : clock10ms_div port map(clock50, clock10ms, clock25);
	-- Set up the VGA controller
	VGA : vga_controller port map(clock108, VGA_hs, vsync, porched, hpos, vpos, VGA_RGB_OUT, darken_rgb);
	-- Map the colours to pin outputs
	VGA_r  <= VGA_RGB_OUT(11 downto 8);
	VGA_g  <= VGA_RGB_OUT(7 downto 4);
	VGA_b  <= VGA_RGB_OUT(3 downto 0);
	VGA_vs <= vsync;

	FLASH : FlashEffect
		port map(clock   => vsync,
			     collide => collision,
			     RGB_in  => vga_rgb,
			     RGB_out => flash_rgb);

	DARKEN : component HyperbeamDarken
		port map(beam_active => beam_active,
			     beam_draw   => hyperbeam_draw,
			     RGB_in      => flash_rgb,
			     RGB_out     => darken_rgb);

	-- Create the background entity
	BG : background port map(hpos, vpos, bg_rgb);

	-- Create the ground
	GROUND_ENTITY : component ground port map(state, vsync, clock108, hpos, vpos, ground_rgb, ground_draw);

	-- Create the bubbles
	BUBBLES : bubble_collection port map(state, vsync, hpos, vpos, bubble_rgb, PRNG_output, bubble_draw);

	-- Create the Pipes
	PIPES : component pipe_collection port map(state, vsync, clock108, hyperbeam_draw, karp_xpos, hpos, vpos, pipe_rgb, pipe_draw, score_trigger);

	-- Create Karp
	KARP_ENTITY : Karp port map(state, vsync, clock108, left_button, hpos, vpos, karp_xpos, karp_ypos, karp_rgb, karp_draw);

	-- Create Hyperbeam ability
	HYPERBEAM_ENTITY : Hyperbeam port map(state, vsync, clock108, powerup_ready or dip_infinitePowerup, hpos, vpos, karp_xpos, karp_ypos, right_button, hyperbeam_rgb, hyperbeam_draw, beam_active);

	-- Random Number Generator
	PRNG : TenBitPRNG generic map("0010010010") port map(clock10ms, PRNG_reset, PRNG_output);

	-- Create the mouse controller
	MOUSE_CONTROL : MOUSE port map(clock25, mouse_reset, mouse_data, mouse_clk, left_button, right_button, mouse_cursor_row, mouse_cursor_column);

	-- Create score displayedScore
	SCORE_DISPLAY_PLAYING : ScoreDisplay generic map(200, 20, 2, 2) port map(clock108, hpos, vpos, score, scoreDisplayPlaying_draw);
	SCORE_TEXT_PLAYING : StringDisplay generic map("SCORE", 20, 20, 2) port map(clock108, hpos, vpos, scoreTextPlaying_draw);

	SCORE_DISPLAY_GAMEOVER : ScoreDisplay generic map(685, 300, 2, 2) port map(clock108, hpos, vpos, score, scoreDisplayGameover_draw);
	SCORE_TEXT_GAMEOVER : StringDisplay generic map("SCORE", 505, 300, 2) port map(clock108, hpos, vpos, scoreTextGameover_draw);

	TITLE : StringDisplay
		generic map(message    => "FLAPPY KARP",
			        x_position => 310,
			        y_position => 200,
			        scale      => 4)
		port map(clk  => clock108,
			     hpos => hpos,
			     vpos => vpos,
			     draw => title_draw);

	CLICK_TO_PLAY : StringDisplay
		generic map(message    => "CLICK TO PLAY",
			        x_position => 445,
			        y_position => 300,
			        scale      => 2)
		port map(clk  => clock108,
			     hpos => hpos,
			     vpos => vpos,
			     draw => ctp_draw);

	TUT_STRING : StringDisplay
		generic map(message    => "SET DIP ZERO FOR TUTORIAL",
			        x_position => 325,
			        y_position => 800,
			        scale      => 2)
		port map(clk  => clock108,
			     hpos => hpos,
			     vpos => vpos,
			     draw => tut_draw);

	GAMEOVER_TEXT : StringDisplay
		generic map(message    => "GAMEOVER",
			        x_position => 400,
			        y_position => 200,
			        scale      => 4)
		port map(clk  => clock108,
			     hpos => hpos,
			     vpos => vpos,
			     draw => go_draw);

	PM : Powermeter
		port map(clock            => vsync,
			     pixel_clock      => clock108,
			     powerup_activate => beam_active,
			     state            => state,
			     hpos             => hpos,
			     vpos             => vpos,
			     powerup_ready    => powerup_ready,
			     draw             => pm_draw);

	SOUND_CONTROLLER : component SoundController
		port map(clock50       => clock50,
			     score_trigger => score_trigger,
			     sound         => sound_out);
	sound <= sound_out;

	-- State controller
	process(clock108)
		variable prev_mouse_left : std_logic := '0';
	begin
		if (btn_reset = '0') then
			state <= idle;
		elsif (rising_edge(clock108)) then
			case state is
				when idle =>
					-- On the `rising edge` of the left button, and when the karp is in the middle of the screen
					if (prev_mouse_left = '0' and left_button = '1' and karp_ypos < 601) then
						if (dip_tutorialPlay = '0') then
							startedInPlaying <= '1';
							state            <= playing;
						else
							startedInPlaying <= '0';
							state            <= tutorial;
						end if;
					end if;
				when playing =>
					-- If tutorial play is enabled reset to idle
					if (dip_tutorialPlay = '1') then
						state <= idle;
					-- If a collision is detected
					elsif (collision = '1') then
						state <= gameover;
					elsif (beam_active = '1') then
						state <= powerup;
					end if;
				when gameover =>
					-- On rising edge of the mouse left button and the karp has fallen to the bottom
					if (prev_mouse_left = '0' and left_button = '1' and karp_ypos > 900) then
						state <= idle;
					end if;
				when tutorial =>
					-- If the tutorial play is disable reset to idle
					if (dip_tutorialPlay = '0') then
						state <= idle;
					elsif (beam_active = '1') then
						state <= powerup;
					end if;
				when powerup =>
					if (beam_active = '0') then
						if (startedInPlaying = '1') then
							state <= playing;
						else
							state <= tutorial;
						end if;
					end if;
				when pause =>
					null;
			end case;
			prev_mouse_left := left_button;
		end if;
	end process;

	-- Assign the correct RGB value for the current pixel
	process(clock108)
		variable collision_found, prev_vsync : std_logic;

	begin
		if (rising_edge(clock108)) then
			-- This is essentially just a priority encoded MUX. An if/else seemed most appropriate here
			-- View this as compositing laters, the top layer is at the top of the if/else stack
			if (title_draw = '1' and state = idle) then -- Title
				vga_rgb <= text_colour;
			elsif (ctp_draw = '1' and state = idle) then -- The "Click to play" text on the welcome screen
				vga_rgb <= text_colour;
			elsif (tut_draw = '1' and state = idle) then -- The "Click to play" text on the welcome screen
				vga_rgb <= text_colour;
			elsif ((state = playing or state = tutorial) and (scoreDisplayPlaying_draw = '1' or scoreTextPlaying_draw = '1')) then -- The score on the playing screen
				vga_rgb <= text_colour;
			elsif (state = gameover and (scoreDisplayGameover_draw = '1' or scoreTextGameover_draw = '1')) then -- The score on the gameover screen
				vga_rgb <= text_colour;
			elsif (go_draw = '1' and state = gameover) then -- The "gameover" text
				vga_rgb <= text_colour;
			elsif (pm_draw = '1' and (state = playing or state = tutorial)) then -- The powerup meter bar thing under the score
				vga_rgb <= text_colour;
			elsif (hyperbeam_draw = '1') then -- The hyperbeam
				vga_rgb <= hyperbeam_rgb;
			elsif (karp_draw = '1') then -- The karp
				vga_rgb <= karp_rgb;
			elsif (pipe_draw = '1' and state /= idle) then -- Pipes
				vga_rgb <= pipe_rgb;
			elsif (bubble_draw = '1') then -- Bubbles
				vga_rgb <= bubble_rgb;
			elsif (ground_draw = '1') then -- Ground
				vga_rgb <= ground_rgb;
			else                        -- Background
				vga_rgb <= bg_rgb;
			end if;

			-- Every time we have finished a frame, assign whether we had collided or not
			if (vsync = '1' and prev_vsync = '0') then
				-- We don't want collisions to change when the game is over
				if (state /= gameover) then
					collision <= collision_found;
				end if;
				-- Reset the collision variable
				collision_found := '0';

			-- Check if the karp is drawing at the same time as either the pipe or the ground
			elsif ((ground_draw = '1' or pipe_draw = '1') and karp_draw = '1') then
				collision_found := '1';
			end if;

			prev_vsync := vsync;
		end if;
	end process;

	-- Score controller
	process(vsync)
	begin
		if (rising_edge(vsync)) then
			-- If we are in the idle state, reset the score
			if (state = idle or (state = tutorial and collision = '1')) then
				score <= (others => 0);
			-- when triggered add one to the score and overflow the first digit
			elsif (score_trigger = '1') then
				if (score(1) = 9) then
					score(1) <= 0;
					score(0) <= score(0) + 1;
				else
					score(1) <= score(1) + 1;
				end if;
			end if;
		end if;
	end process;

	lights <= (others => right_button);
end FlappyKarp_arch;