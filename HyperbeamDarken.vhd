library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity HyperbeamDarken is
	port(
		beam_active, beam_draw : in  std_logic;
		RGB_in                 : in  std_logic_vector(11 downto 0);
		RGB_out                : out std_logic_vector(11 downto 0)
	);
end entity HyperbeamDarken;

architecture HyperbeamDarken_arch of HyperbeamDarken is
begin
	darken : process(RGB_in, beam_active, beam_draw) is
		variable r, g, b : std_logic_vector(3 downto 0);
	begin
		-- We need to cap the addition to x"F" so we include an overflow bit
		r := RGB_in(11 downto 8);
		g := RGB_in(7 downto 4);
		b := RGB_in(3 downto 0);
		if (beam_active = '1') then
			if (beam_draw = '0') then
				-- Cap the signal to x"F"
				if r > x"5" then
					r := r - x"5";
				else
					r := x"0";
				end if;

				if g > x"5" then
					g := g - x"5";
				else
					g := x"0";
				end if;

				if b > x"6" then
					b := b - x"6";
				else
					b := x"0";
				end if;
			end if;
		end if;
		-- Reassign the output
		RGB_out <= r(3 downto 0) & g(3 downto 0) & b(3 downto 0);
	end process darken;

end architecture HyperbeamDarken_arch;
