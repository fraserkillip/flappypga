library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity ScoreDisplay is
	generic(
		-- Position to draw the score
		xpos  : integer range 0 to 1280;
		ypos  : integer range 0 to 1024;
		-- Number of numbers to draw
		size  : integer range 0 to 3;
		-- Scale to draw numbers at
		scale : integer range 1 to 4
	);
	port(
		clk   : in  std_logic;
		hpos  : in  integer range 0 to 1280;
		vpos  : in  integer range 0 to 1024;
		score : in  scores;
		draw  : out std_logic
	);
end entity ScoreDisplay;

architecture ScoreDisplay_Arch of ScoreDisplay is
	component NumDisplay is
		generic(
			x_position : INTEGER RANGE 0 TO 1280 := 0;
			y_position : INTEGER RANGE 0 TO 1024 := 0;
			scale      : integer range 1 to 4
		);
		port(
			clk  : in  std_logic;
			hpos : IN  INTEGER RANGE 0 TO 1280 := 0;
			vpos : IN  INTEGER RANGE 0 TO 1024 := 0;
			num  : integer range 0 to 9;
			draw : out std_logic
		);
	end component NumDisplay;

	-- Determine number spacing
	constant dx : integer := 15 * scale;

	-- Number draw signals
	signal draw_vector : std_logic_vector(size - 1 downto 0);
begin
	-- Generate score characters
	score_chars : for i in 0 to size - 1 generate
		char : NumDisplay
			generic map(x_position => xpos + i * dx,
				        y_position => ypos,
				        scale      => scale)
			port map(clk  => clk,
				     hpos => hpos,
				     vpos => vpos,
				     num  => score(i),
				     draw => draw_vector(i));
	end generate;

	-- Determine if any number wants to draw at the current draw position
	draw <= '1' when draw_vector /= (draw_vector'range => '0') else '0';

end architecture ScoreDisplay_Arch;
