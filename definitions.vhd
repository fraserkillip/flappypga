package definitions is
	-- Game states
	type game_state is (idle, playing, gameover, tutorial, powerup, pause);
	-- Score array
	type scores is array (1 downto 0) of integer range 0 to 9;
end package definitions;

package body definitions is
	
end package body definitions;
