library ieee;
use ieee.std_logic_1164.all;

entity TenBitPRNG is
	generic(
		-- Starting state of each bit
		iv : std_logic_vector(9 downto 0)
	);
	port(
		clock, reset : in  std_logic;
		output       : out std_logic_vector(9 downto 0)
	);
end entity TenBitPRNG;

architecture TenBitPRNGArch of TenBitPRNG is
	---------------------------------------------------------
	signal values : std_logic_vector(9 downto 0) := iv;
	signal input  : std_logic;
	---------------------------------------------------------
	component RightShiftRegister is
		generic(
			iv : std_logic_vector
		);
		port(
			clock, reset, input : in  std_logic;
			output              : out std_logic_vector(9 downto 0)
		);
	end component RightShiftRegister;
---------------------------------------------------------
begin
	SREG : RightShiftRegister generic map(iv) port map(clock, reset, input, values);

	-- Determine input required for random number gen
	input <= (((values(9) XOR values(8)) XOR values(6)) XOR values(5));

	-- Output values
	output <= values;

end architecture TenBitPRNGArch;
