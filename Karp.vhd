library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

use work.definitions.all;

entity Karp is
	port(
		state                      : in  game_state;
		clk, rom_clock, mouse_left : in  std_logic;
		hpos                       : in  integer range 0 to 1280;
		vpos                       : in  integer range 0 to 1024;
		xpos_out, ypos_out         : out integer range 0 to 1280;
		RGB                        : out std_logic_vector(11 downto 0);
		draw                       : out std_logic
	);
end entity Karp;

architecture Karp_Arch of Karp is
	-- Maximum number of pixels the karp can move in one cycle
	constant terminalVel : integer := 30;
	-- Amount to increase velocity by each cycle
	constant gravity     : integer := 1;
	-- The velocity to set the karp to when it jumps
	constant jumpVel     : integer := -25;

	-- X position of the karp on the screen
	constant xpos : integer range 0 to 1280     := 1280 / 2 - 50; -- ~Centre the karp
	-- Double Y position of the karp on the screen
	signal ypos   : integer range 0 to 1024 * 2 := (1024 / 2 - 50) * 2; -- ~Centre the karp

	-- Current number of pixels per cycle the karp is moving
	signal yvel : integer range jumpVel to terminalVel;

	-- Stores bounds of karp, '1' if draw position is in bounds else '0'
	signal in_bounds : std_logic := '0';

	-- Rom signals
	SIGNAL rom_data    : STD_LOGIC_VECTOR(12 DOWNTO 0);
	SIGNAL rom_address : STD_LOGIC_VECTOR(12 DOWNTO 0);

	COMPONENT altsyncram
		GENERIC(
			address_aclr_a         : STRING;
			clock_enable_input_a   : STRING;
			clock_enable_output_a  : STRING;
			init_file              : STRING;
			intended_device_family : STRING;
			lpm_hint               : STRING;
			lpm_type               : STRING;
			numwords_a             : NATURAL;
			operation_mode         : STRING;
			outdata_aclr_a         : STRING;
			outdata_reg_a          : STRING;
			widthad_a              : NATURAL;
			width_a                : NATURAL;
			width_byteena_a        : NATURAL
		);
		PORT(
			clock0    : IN  STD_LOGIC;
			address_a : IN  STD_LOGIC_VECTOR(12 DOWNTO 0);
			q_a       : OUT STD_LOGIC_VECTOR(12 DOWNTO 0)
		);
	END COMPONENT;

begin
	altsyncram_component : altsyncram
		GENERIC MAP(
			address_aclr_a         => "NONE",
			clock_enable_input_a   => "BYPASS",
			clock_enable_output_a  => "BYPASS",
			init_file              => "karp.mif",
			intended_device_family => "Cyclone III",
			lpm_hint               => "ENABLE_RUNTIME_MOD=NO",
			lpm_type               => "altsyncram",
			numwords_a             => 6864,
			operation_mode         => "ROM",
			outdata_aclr_a         => "NONE",
			outdata_reg_a          => "UNREGISTERED",
			widthad_a              => 13,
			width_a                => 13,
			width_byteena_a        => 1
		)
		PORT MAP(
			clock0    => rom_clock,
			address_a => rom_address,
			q_a       => rom_data
		);

	-- Moves the karp every cycle and listens for the mouse click signal
	process(clk)
		-- Mouse click signal from the previous cycle
		variable prev_mouse    : std_logic;
		-- '1' after we have done the gameover jump
		variable gameover_jump : std_logic := '0';
	begin
		-- Sensitive on clk (60Hz)
		if rising_edge(clk) then
			if (state /= powerup) then
				-- Move the karp by yvel and check it does not leave the screen
				if ((ypos + yvel) > 946 * 2) then
					ypos <= 946 * 2;
				elsif ((ypos + yvel) < 5) then
					ypos <= 5;
				else
					ypos <= ypos + yvel;
				end if;

				-- Apply gravity to yvel and keep yvel below terminalVel
				if ((yvel + gravity) > terminalVel) then
					yvel <= terminalVel;
				else
					yvel <= yvel + gravity;
				end if;

				-- If we have left the gameover state...
				if (state /= gameover) then
					-- Reset gameover_jump flag
					gameover_jump := '0';
				end if;

				-- Make the karp jump
				if ((mouse_left = '1' and prev_mouse = '0' and (state = playing or state = tutorial)) or (state = idle and ypos > 1200)) then
					-- Mouse pressed in a playing state...
					-- Or in idle state and fell below min hight
					yvel <= jumpVel;
				elsif (state = gameover and gameover_jump = '0') then
					-- Gameover jump!
					yvel          <= jumpVel;
					-- Make sure we only jump once
					gameover_jump := '1';
				end if;

				prev_mouse := mouse_left;
			end if;
		end if;
	end process;

	-- Determine if we are in the karps bounds
	in_bounds <= '1' when xpos <= hpos and xpos + 88 > hpos and ypos / 2 <= vpos and ypos / 2 + 78 > vpos else '0';
	-- Determine if we should draw the karp
	-- in_bounds and image alpha = '1'
	draw      <= in_bounds and rom_data(12);

	process(rom_clock)
		-- Current position in the rom
		variable position  : std_logic_vector(12 downto 0) := "0000000000000";
		-- Current count of pixels read on the current line of the image
		variable linecount : integer range 0 to 88         := 0;
	begin
		if (rising_edge(rom_clock)) then
			-- When we reach the end of the image...
			if (position = "1101011010000") then
				-- Reset position
				position := "0000000000000";
			end if;
			-- Set position of the rom
			rom_address <= position;
			-- Reset linecount on the left side of the image
			if (linecount >= 88 AND xpos = hpos) then
				linecount := 0;
			end if;

			-- Increment position while in_bounds and within width of image
			if (in_bounds = '1' AND linecount < 88) then
				position  := position + 1;
				linecount := linecount + 1;
			end if;
		end if;
	end process;

	-- Output RGB data
	RGB      <= rom_data(11 downto 0);
	-- Output karp position
	xpos_out <= xpos;
	ypos_out <= ypos / 2;
end architecture Karp_Arch;
