library ieee;
use ieee.std_logic_1164.all;

entity SoundController is
	port(
		clock50, score_trigger : in  std_logic;
		sound                  : out std_logic
	);
end entity SoundController;

architecture SoundController_arch of SoundController is
	signal clock44m, clock44k : std_logic;

	component clock44 is
		port(
			clock_in_clk  : in  std_logic := 'X'; -- clk
			clock_out_clk : out std_logic; -- clk
			reset_reset   : in  std_logic := 'X' -- reset
		);
	end component clock44;

	component MifSoundPlayer is
		generic(
			rom_name      : string;
			num_words     : integer;
			address_width : integer
		);
		port(
			clock, sample_clock : in  std_logic;
			play, loop_play     : in  std_logic := '0';
			sound               : out std_logic
		);
	end component MifSoundPlayer;
begin
	-- Generate the 44.1MHz clock (actually 44.090909Mhz)
	clock_pll : component clock44
		port map(
			clock_in_clk  => clock50,   --  clock_in.clk
			clock_out_clk => clock44m, -- clock_out.clk
			reset_reset   => '0'        --     reset.reset
		);

	sound_clock : process(clock44m) is
		variable clock_div : integer range 0 to 500 := 0;
	begin
		if (rising_edge(clock44m)) then
			if (clock_div = 500) then
				clock44k <= not clock44k;
				clock_div := 0;
			end if;
			clock_div := clock_div + 1;
		end if;
	end process;
	
	sine_440: MifSoundPlayer
		generic map(rom_name      => "1up.mif",
			        num_words     => 6837,
			        address_width => 13)
		port map(clock        => clock50,
			     sample_clock => clock44k,
			     play         => score_trigger,
			     loop_play    => '0',
			     sound        => sound);

end architecture SoundController_arch;
