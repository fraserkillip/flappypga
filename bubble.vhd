library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.definitions.all;

entity BUBBLE is
	generic(
		init_delay : in integer range 0 to 1024
	);
	port(
		state : in  game_state;
		clk   : in  std_logic;
		hpos  : in  integer range 0 to 1280;
		vpos  : in  integer range 0 to 1024;
		prng  : in  std_logic_vector(9 downto 0);
		draw  : out std_logic := '0'
	);
end bubble;

ARCHITECTURE BUBBLE_ARCH OF BUBBLE IS
	-- Define type for storing bubble draw map
	TYPE bubble_map IS ARRAY (5 downto 0, 5 downto 0) OF std_logic;
	-- Define bubble draw map
	CONSTANT b_map : bubble_map := ("001100",
		                             "011110",
		                             "110011",
		                             "110011",
		                             "011110",
		                             "001100");
	-- x/y positions of bubble
	SIGNAL xpos            : integer range 0 to 1280;
	SIGNAL ypos            : integer range 0 to 1024 := 0;
	-- Delays the bubble from appearing on the screen
	signal remaining_delay : integer range 0 to 1024 := init_delay;
BEGIN
	-- Update draw signal
	process(hpos, vpos, xpos, ypos)
		variable xVal, yVal : integer range -512 to 511;
	begin
		-- Calculate current x/yVals
		xVal := (hpos - xpos);
		yVal := (vpos - ypos);

		-- Check if the position is within the bubbles bounds
		if ((xVal >= 0 AND xVal < 6) AND (yVal >= 0 AND yVal < 6)) then
			-- Set draw to value of draw map
			draw <= b_map(xVal, yVal);
		else
			-- Reset draw to '0'
			draw <= '0';
		end if;
	end process;

	-- Update the bubbles location
	process(clk)
		variable time_flowing : std_logic;
	begin
		if (rising_edge(clk)) then
			-- If time and the karp are both moving..
			if ((state /= powerup and state /= pause) and state /= gameover) then
				-- Move the bubbles x position
				xpos <= xpos - 5;
			end if;
			
			if (state /= powerup and state /= pause) then
				if (remaining_delay > 0) then
					remaining_delay <= remaining_delay - 1;
				elsif (ypos = 0) then -- Once the bubble hits the top..
					-- Reset the bubble to the bottom of the ocean floor
					ypos <= 960;
					-- Get a random x position
					xpos <= to_integer(unsigned(prng));
				else
					-- Move the bubble up some more
					ypos <= ypos - 1;
				end if;
			end if;
		end if;
	end process;

end bubble_arch;